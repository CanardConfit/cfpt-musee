﻿/*
 * Projet : Musée
 * Auteur : Tom Andrivet
 */
using System.Collections.Generic;
using System.Text;

namespace cfpt_musee
{
    /// <summary>
    /// Classe représentant un cone du musée.
    /// </summary>
    public class Cone
    {
        /// <summary>
        /// Caractère utilisé pour les espaces de la forme.
        /// </summary>
        public const char CHAR_SPACE = ' ';

        /// <summary>
        /// Caractère utilisé pour la barre du bas en orientation haut.
        /// </summary>
        public const char CHAR_UNDERSCORE = '_';

        /// <summary>
        /// Caractère utilisé pour la barre du haut en orientation bas.
        /// </summary>
        public const char CHAR_UPPERSCORE = '¯';

        /// <summary>
        /// Caractère utilisé pour les cotés du cone.
        /// </summary>
        public const char CHAR_SLASH = '/';

        /// <summary>
        /// Caractère utilisé pour les cotés du cone.
        /// </summary>
        public const char CHAR_BACKSLASH = '\\';

        /// <summary>
        /// Fonction qui permet de récupérer les lignes a afficher.
        /// </summary>
        /// <param name="cone">Cone a transformer.</param>
        /// <param name="rangee">Nombre de rangée (Hauteur du musée).</param>
        /// <param name="isHaut">Boolean qui défini si la pointe est vers le haut ou le bas.</param>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        public static List<string> GetMusee(Cone cone, int rangee, bool isHaut)
        {
            // Récuperation du musé initial
            List<string> ret = GetBaseMusee(cone, rangee);

            if (!isHaut) // inversion des symboles et lignes
            {
                List<string> retBas = new List<string>();

                for (int iRet = ret.Count - 1; iRet >= 0; iRet--)
                {
                    retBas.Add(InvertSymboles(ret[iRet]));
                }

                return retBas;
            }

            return ret;
        }

        /// <summary>
        /// Fonction qui permet de générer la liste chainée de cone.
        /// </summary>
        /// <param name="num">Index de hauteur de la liste.</param>
        /// <param name="rangee">Nombre de rangée (Hauteur du musée).</param>
        /// <param name="dim">Dimention des cones.</param>
        /// <param name="doubleCheck">Boolean qui défini si le coté gauche doit être créer dans la tour.</param>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        public static Cone GenerateCone(int num, int rangee, int dim, bool doubleCheck)
        {
            Cone ret;

            if (num < rangee) // Nous ne sommes pas arrivé en bas du musée, appel a soi même
                ret = new Cone(doubleCheck ? GenerateCone(num + 1, rangee, dim, true) : null, GenerateCone(num + 1, rangee, dim, false), dim);
            else // Bas du musée, pas d'enfant
                ret = new Cone(null, null, dim);

            return ret;
        }

        /// <summary>
        /// Fonction qui permet de changer les symboles, pour le retournement du musée.
        /// </summary>
        /// <param name="str">Le string contenant les symboles.</param>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        private static string InvertSymboles(string str)
        {
            StringBuilder sb = new StringBuilder(str);
            for (int i = 0; i < sb.Length; i++) // Parcour du string au lieu d'un replace pour évité que tous les symboles soit du meme sens.
            {
                if (sb[i] == CHAR_BACKSLASH)
                    sb[i] = CHAR_SLASH;
                else if (sb[i] == CHAR_SLASH)
                    sb[i] = CHAR_BACKSLASH;
                else if (sb[i] == CHAR_UNDERSCORE)
                    sb[i] = CHAR_UPPERSCORE;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Fonction permettant de récupérer le musée aligné initial (vers le haut).
        /// </summary>
        /// <param name="cone">Cone a transformer.</param>
        /// <param name="rangee">Nombre de rangée du musée.</param>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        private static List<string> GetBaseMusee(Cone cone, int rangee)
        {
            // Récupération de l'affichage simple des cones
            List<string> ret = cone.GetToPrint();

            // Ajout de l'espacement pour l'alignement (Des deux cotés >> Carré)
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = GetSpacesHorizontal(cone.Dim, rangee, i / cone.Dim) + ret[i] + GetSpacesHorizontal(cone.Dim, rangee, i / cone.Dim);
            }
            return ret;
        }

        /// <summary>
        /// Fonction qui permet de récupérer l'espacement nécessaire pour les cones (Espacement pour les aligner)
        /// </summary>
        /// <param name="dim">Dimention du cone.</param>
        /// <param name="rangee">Nombre de rangée du musée.</param>
        /// <param name="index">Index de hauteur (rangée) du cone.</param>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        private static string GetSpacesHorizontal(int dim, int rangee, int index)
        {
            StringBuilder sb = new StringBuilder();

            for (int space = 0; space < (rangee - 1) * dim - index * dim; space++)
                sb.Append(CHAR_SPACE);

            return sb.ToString();
        }

        /// <summary>
        /// Cone droite ou gauche enfants.
        /// </summary>
        private Cone _droite, _gauche;

        /// <summary>
        /// Dimention du cone.
        /// </summary>
        private int _dim;

        /// <summary>
        /// Permet de récuperer le Cone droit.
        /// </summary>
        public Cone Droite
        {
            get => _droite;
        }

        /// <summary>
        /// Permet de récuperer le Cone gauche.
        /// </summary>
        public Cone Gauche
        {
            get => _gauche;
        }

        /// <summary>
        /// Permet de récuperer la taille du Cone.
        /// </summary>
        public int Dim
        {
            get => _dim;
        }

        /// <summary>
        /// Constructeur de Cone.
        /// </summary>
        /// <param name="droite">Cone enfant droit.</param>
        /// <param name="gauche">Cone enfant gauche.</param>
        /// <param name="dim">Dimention du cone.</param>
        public Cone(Cone droite, Cone gauche, int dim)
        {
            _droite = droite;
            _gauche = gauche;
            _dim = dim;
        }

        /// <summary>
        /// Fonction qui permet de récuperer l'affichage du Musée (Non aligné, juste les cones).
        /// </summary>
        /// <returns>Une liste de ligne contenant le musée.</returns>
        public List<string> GetToPrint()
        {
            List<string> ret = new List<string>();

            ret.AddRange(GetCone());

            // Ajout des enfants si il y en a
            if (_droite != null && _gauche != null)
                ret.AddRange(JoinHorizontal(_droite.GetToPrint(), _gauche.GetToPrint()));
            else if (_droite != null && _gauche == null)
                ret.AddRange(_droite.GetToPrint());
            else if (_droite == null && _gauche != null)
                ret.AddRange(_gauche.GetToPrint());

            return ret;
        }

        /// <summary>
        /// Fonction qui permet de rassembler deux listes de lignes sur les mêmes lignes.
        /// </summary>
        /// <param name="first">Première liste.</param>
        /// <param name="second">Deuxième liste.</param>
        /// <returns>Une liste de ligne contenant les cones joints.</returns>
        private List<string> JoinHorizontal(List<string> first, List<string> second)
        {
            for (int i = 0; i < first.Count; i++)
            {
                first[i] += second[i];
            }

            // Ajout des elements de la liste restant dans second (Pas utilisé dans le contexte des cones, mais au cas ou)
            for (int i = first.Count; i < second.Count; i++)
            {
                first.Add(second[i]);
            }

            return first;
        }

        /// <summary>
        /// Fonction qui permet de récupérer l'affichage du cone.
        /// </summary>
        /// <returns>Une liste de ligne contenant le cone.</returns>
        public List<string> GetCone()
        {
            List<string> ret = new List<string>();

            StringBuilder spaces = new StringBuilder();

            // Création de l'espace a mettre pour que tout soit aligné
            for (int i = 1; i < _dim; i++)
                spaces.Append(CHAR_SPACE);

            for (int d = 0; d < _dim; d++)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"{spaces}{CHAR_SLASH}");

                for (int space = 0; space < d*2; space++)
                {
                    // Affichage du sol si c'est la dernière itération
                    sb.Append(d + 1 == _dim ? CHAR_UNDERSCORE : CHAR_SPACE);
                }
                sb.Append($"{CHAR_BACKSLASH}{spaces}");

                // Enlevement d'un espace a chaque itération
                if (spaces.Length > 0)
                    spaces.Remove(0, 1);

                ret.Add(sb.ToString());
            }

            return ret;
        }
    }
}
