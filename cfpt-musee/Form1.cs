﻿/*
 * Projet : Musée
 * Auteur : Tom Andrivet
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace cfpt_musee
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Sauvegarde du cone actuellement générer.
        /// </summary>
        private Cone _cone;

        /// <summary>
        /// Constructeur de la form.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            Form1_Resize(null, null);
        }

        /// <summary>
        /// Evenement quand l'on clique sur les boutons
        /// </summary>
        private void btns_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.Name)
            {
                case "btnConstruire":
                    int dim = (int)nudDim.Value;
                    int rangee = (int)nudrangee.Value;
                    bool isHaut = rbtnHaut.Checked;

                    // Generation de la liste chainée
                    _cone = Cone.GenerateCone(0, rangee-1, dim, true);

                    // Affichage de la liste chainée.
                    lbxConstruct.DataSource = Cone.GetMusee(_cone, rangee, isHaut);
                    break;
            }
        }

        /// <summary>
        /// Evenement quand la form change de taille.
        /// </summary>
        private void Form1_Resize(object sender, EventArgs e)
        {
            lbxConstruct.Size = new Size(Size.Width - 30, Size.Height - 140);
        }
    }
}