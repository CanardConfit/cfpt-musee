﻿namespace cfpt_musee
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnBas = new System.Windows.Forms.RadioButton();
            this.rbtnHaut = new System.Windows.Forms.RadioButton();
            this.nudDim = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudrangee = new System.Windows.Forms.NumericUpDown();
            this.btnConstruire = new System.Windows.Forms.Button();
            this.lbxConstruct = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudrangee)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnBas);
            this.groupBox1.Controls.Add(this.rbtnHaut);
            this.groupBox1.Location = new System.Drawing.Point(7, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(75, 71);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Orientation";
            // 
            // rbtnBas
            // 
            this.rbtnBas.AutoSize = true;
            this.rbtnBas.Location = new System.Drawing.Point(6, 42);
            this.rbtnBas.Name = "rbtnBas";
            this.rbtnBas.Size = new System.Drawing.Size(43, 17);
            this.rbtnBas.TabIndex = 1;
            this.rbtnBas.TabStop = true;
            this.rbtnBas.Text = "Bas";
            this.rbtnBas.UseVisualStyleBackColor = true;
            // 
            // rbtnHaut
            // 
            this.rbtnHaut.AutoSize = true;
            this.rbtnHaut.Checked = true;
            this.rbtnHaut.Location = new System.Drawing.Point(6, 19);
            this.rbtnHaut.Name = "rbtnHaut";
            this.rbtnHaut.Size = new System.Drawing.Size(48, 17);
            this.rbtnHaut.TabIndex = 0;
            this.rbtnHaut.TabStop = true;
            this.rbtnHaut.Text = "Haut";
            this.rbtnHaut.UseVisualStyleBackColor = true;
            // 
            // nudDim
            // 
            this.nudDim.Location = new System.Drawing.Point(242, 29);
            this.nudDim.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudDim.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudDim.Name = "nudDim";
            this.nudDim.Size = new System.Drawing.Size(57, 20);
            this.nudDim.TabIndex = 1;
            this.nudDim.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Dimension";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nombre de rangées";
            // 
            // nudrangee
            // 
            this.nudrangee.Location = new System.Drawing.Point(242, 59);
            this.nudrangee.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudrangee.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudrangee.Name = "nudrangee";
            this.nudrangee.Size = new System.Drawing.Size(57, 20);
            this.nudrangee.TabIndex = 4;
            this.nudrangee.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // btnConstruire
            // 
            this.btnConstruire.Location = new System.Drawing.Point(363, 12);
            this.btnConstruire.Name = "btnConstruire";
            this.btnConstruire.Size = new System.Drawing.Size(177, 37);
            this.btnConstruire.TabIndex = 5;
            this.btnConstruire.Text = "Construire";
            this.btnConstruire.UseVisualStyleBackColor = true;
            this.btnConstruire.Click += new System.EventHandler(this.btns_Click);
            // 
            // lbxConstruct
            // 
            this.lbxConstruct.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxConstruct.FormattingEnabled = true;
            this.lbxConstruct.Location = new System.Drawing.Point(7, 102);
            this.lbxConstruct.Name = "lbxConstruct";
            this.lbxConstruct.Size = new System.Drawing.Size(533, 342);
            this.lbxConstruct.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 450);
            this.Controls.Add(this.lbxConstruct);
            this.Controls.Add(this.btnConstruire);
            this.Controls.Add(this.nudrangee);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudDim);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(568, 489);
            this.Name = "Form1";
            this.Text = "Construction de musées";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudrangee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnBas;
        private System.Windows.Forms.RadioButton rbtnHaut;
        private System.Windows.Forms.NumericUpDown nudDim;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudrangee;
        private System.Windows.Forms.Button btnConstruire;
        private System.Windows.Forms.ListBox lbxConstruct;
    }
}